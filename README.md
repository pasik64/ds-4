# DS-4

## Popis uzlů
Aplikace obshahu 3 typy kontejnerů: zookeeper uzly, mosquitto uzly a klientské uzly. Princip jejich funkcionality je popsán v souboru ds3.pdf.


## Jak spustit

V kořenovém adresáři projektu (obsahuje Vagrantfile) stačí zadat příkaz: vagrant up, čímž se postupně začnou spouštět jednotlivé kontejnery.

## Ověření funkčnosti
Každý uzel obsahuje logování. U klientských uzlů se v logách objeví informace, zda daný klient zprávy generuje (zobrazí se ,,I am spammer" nebo přijímá ,,I am receiver"). Při každé obržené testovací zprávě se zpráva vypíše v logu i s názvem topicu. Mosquitto uzly informují o každé přijaté zprávě stejně jako klientské uzly.

