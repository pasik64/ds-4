import os
from kazoo.client import KazooClient
from paho.mqtt import client
import time
from time import sleep
import threading
import socket

def message_received(client, userdata, message):
    topic = message.topic
    text = message.payload.decode('UTF-8')
    print("Message received - topic: " + topic + "; message: " + text, flush=True)
    if not text.startswith("rpl_"):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        sock.sendto(bytes(topic + "_SEP_" + text, 'utf-8'), ("255.255.255.255", 12345))

def connect(client, userdata, flags, rc):
    client.subscribe("#")

def disconnect(client, userdata, rc):
    print("Mosquitto disconnect. Rc: " + rc, flush=True)
    exit(rc)

def message_loop_thread(client_param: client.Client):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    sock.bind(("", 12345))
    while True:
        data, addr = sock.recvfrom(1024)
        text = data.decode('UTF-8')
        msg_split = text.split("_SEP_")
        external = (socket.gethostbyname(socket.gethostname()) is addr)
        if external and len(msg_split) is 2:
            client_param.publish(msg_split[0], bytes("rpl_"+msg_split[1], 'utf-8'))


print("Initializing Zookeeper", flush=True)
zoo = KazooClient(hosts=os.environ['ZOO_SERVERS'])
zoo.start()
zoo.create("/cluster/c00/node", value=bytes(os.environ["NODE_IP"], 'utf-8'), ephemeral=True, sequence=True, makepath=True)


print("Initializing mosquitto", flush=True)
mosquitto = client.Client()
mosquitto.on_message = message_received
mosquitto.on_connect = connect
mosquitto.on_disconnect = disconnect
mosquitto.connect(os.environ["NODE_IP"])


print("Starting loop", flush=True)
loop_thread = threading.Thread(target=message_loop_thread, args=(mosquitto,), daemon=True)
loop_thread.start()
mosquitto.loop_forever()
