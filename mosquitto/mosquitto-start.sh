#!/bin/sh

set -e
/usr/sbin/mosquitto -c /mosquitto/config/mosquitto.conf &

echo "Starting mosquitto script"
python3 -u /opt/mosquitto/main.py
