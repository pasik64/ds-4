import os
import socket
from kazoo.client import KazooClient
from paho.mqtt import client
import time
from time import sleep
import random

def message_received(client, userdata, message):
    print("Message received - topic: " + message.topic + "; message: " + message.payload.decode('UTF-8'), flush=True)

def connect(client, userdata, flags, rc):
    if rc != 0:
        print("Unable to connect to Mosquitto. Rc "+rc, flush=True)
        client.loop_stop()

def disconnect(client, userdata, rc):
    print("Disconnected from Mosquitto. Rc: " + rc, flush=True)
    client.loop_stop()

print("Client started", flush=True)
sleep(20)
print("Zookeeper initialization", flush=True)
hosts = os.environ['ZOO_SERVERS']
zoo = KazooClient(hosts=hosts)
zoo.start()


spammer = random.choice([True, False])
if spammer:
    print("I am spammer")
else:
    print("I am receiver")  

while True:
    nodes = zoo.get_children("/cluster/c00")
    nodes_count = len(nodes)
    if nodes_count < 1:
        exit(-1)
    node_index = random.randrange(nodes_count)
    node = zoo.get("/cluster/c00/"+nodes[node_index])
    mosquitto = client.Client()
    mosquitto.on_message = message_received
    mosquitto.on_connect = connect
    mosquitto.on_disconnect = disconnect
    mosquitto.connect(node[0].decode("UTF-8"))
    print("Connected to: " + node[0].decode("UTF-8"), flush=True)
    if spammer:
        mosquitto.publish("test-topic","Test message")
        sleep(5)
    else:
        mosquitto.subscribe("test-topic")
        mosquitto.loop_forever()
        sleep(5)
